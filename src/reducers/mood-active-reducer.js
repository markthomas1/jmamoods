import { SELECT_MOOD } from '../actions/constants'

export default function(state = null, action) {
    switch(action.type) {
        case SELECT_MOOD:
            return action.payload
    }
    return state
}