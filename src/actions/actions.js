import {
    SELECT_MOOD,
    SET_GLOBAL_BACKGROUND
} from './constants'

//each action should have the following signiture.
//  {
//     type: <type of action>,        type is required
//     payload: <the actual payload>  payload is optional. if you don't
//                                    have anything to send to reducer,
//                                    you don't need the payload. for example
//                                    newCounter action
//  }

//this action tell the reducer which counter with specified id needs to be
//incremented.
export const selectMood = (mood) => {
    return {
        type: SELECT_MOOD,
        payload: mood
    }
}


//this action tell the reducer what the global background should be
export const setGlobalBackground = (color) => {
    return {
        type: SET_GLOBAL_BACKGROUND,
        payload: color
    }
}


