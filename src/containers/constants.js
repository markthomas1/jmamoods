import {
    Platform
} from 'react-native'

export const FONTFAMILY = (Platform.OS === 'ios') ? 'Georgia' : 'serif'
export const GREY = '#F5F5F5'
export const WHITE = '#FFFFFF'
export const BLACK_TEXT_COLOR = '#505254'
export const GREY_TEXT_COLOR = '#A7A8A9'
export const GREEN = '#8BC129'
export const MOOD_ITEM_HEIGHT = 96
export const MOOD_ITEM_WIDTH = 70