'use strict'

import React, {
    Component,
    StyleSheet,
    View,
    Text,
    Image
} from 'react-native'

import ActionButton from 'react-native-action-button'
import Icon from 'react-native-vector-icons/Ionicons'
import Moment from 'moment'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { setGlobalBackground } from '../actions/actions'
import * as C from './constants'

class MoodDetail extends Component {

    componentWillMount() {
        this.props.setGlobalBackground(C.GREY);
    }

    componentWillUnmount() {
        this.props.setGlobalBackground(C.WHITE);
    }

    render() {

        if(!this.props.moodActive) {
            return <Text>Select a mood to get started.</Text>
        }

        const {dateTime, feeling, origin, energy, thoughts} = this.props.moodActive;
        const formattedDateTime = Moment(dateTime).format('dddd, MMMM DD | h:mm a').toUpperCase();

        return (
            <View style={[styles.container]}>
                <View style={[styles.content, this.border('red')]}>
                    <Text style={styles.date}>{formattedDateTime}</Text>
                    <Text style={styles.feeling}>{feeling}</Text>

                    <Icon.Button
                        name="ios-ionic-outline"
                        color={C.BLACK_TEXT_COLOR}
                        backgroundColor="transparent"
                        size={26}
                        padding={0}
                        iconStyle={{paddingTop: 4}}
                    >
                        <Text style={styles.regularText}>{origin}</Text>
                    </Icon.Button>

                    <Icon.Button
                        name="ios-plus-outline"
                        color={C.BLACK_TEXT_COLOR}
                        backgroundColor="transparent"
                        size={26}
                        padding={0}

                        iconStyle={{paddingTop: 4}}
                    >
                        <Text style={styles.regularText}>{energy}</Text>
                    </Icon.Button>

                    <Text style={styles.subheading}>THOUGHTS THAT CREATED THIS MOOD</Text>
                    <Text style={styles.regularText} numberOfLines={6}>{thoughts}</Text>
                </View>
                <View style={[styles.footer, this.border('blue')]}>
                    <Image
                        style={styles.background}
                        source={require('../assets/mood-detail-background.jpg')}
                    />
                </View>
                <ActionButton
                    buttonColor={C.GREEN}
                    position='center'
                    offsetY={16}
                    icon={<Text style={styles.actionButtonText}>EDIT</Text>}
                    onPress={()=>{console.log('new entry clicked')}}
                >

                </ActionButton>
            </View>
        )
    }

    border(color) {
        return {
            borderColor: color,
            borderWidth: 0
        }
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: C.GREY
    },
    content: {
        flex: 1,
        paddingLeft: 14,
        paddingRight: 14
    },
    footer: {
        flex: 1,
    },
    background: {
        flex: 1,
        resizeMode: 'stretch',
        // remove width and height to override fixed static size
        width: null,
        height: null,
    },
    date: {
        color: C.BLACK_TEXT_COLOR,
        fontWeight: 'bold',
    },
    feeling: {
        color: C.BLACK_TEXT_COLOR,
        fontFamily: C.FONTFAMILY,
        fontSize: 28,
        fontWeight: 'bold',
        paddingTop: 10,
        paddingBottom: 6
    },
    regularText: {
        fontSize: 18,
        color: C.BLACK_TEXT_COLOR,
    },
    subheading: {
        paddingTop: 8,
        paddingBottom: 8,
        color: C.GREY_TEXT_COLOR,
        fontWeight: 'bold',
    },
    actionButtonText: {
        color: C.WHITE,
        fontWeight: 'bold'
    }
})


function mapStateToProps(state) {
    return {
        moodActive: state.moodActive,
        global: state.global,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setGlobalBackground: setGlobalBackground}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MoodDetail)