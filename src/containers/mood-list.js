'use strict'

import React, {
    Component,
    StyleSheet,
    View,
    Text,
    ListView,
    RecyclerViewBackedScrollView,
    TouchableHighlight
} from 'react-native'

import Moment from 'moment'
import Swipeout from 'react-native-swipeout'
import Tabs from 'react-native-tabs'
import ActionButton from 'react-native-action-button'
import Icon from 'react-native-vector-icons/Ionicons'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import {
    selectMood,
    setGlobalBackground
} from '../actions/actions'

import * as C from './constants'

// ios-person-outline for Self-Directed
// ios-sunny-outline for Event-Driven

class MoodList extends Component {

    constructor(props) {
        super(props);

        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.generateRows({})),
            tab: 'history'
        };
    }

    generateRows = (pressData) => {
        var dataBlob = this.props.moods.map((mood) => {
            return mood;
        })
        return dataBlob;
    }

    renderRow = (rowData, sectionID, rowID) => {

        const swipeoutBtns = [
            {
                text: 'BUST OUT',
                backgroundColor: '#FFFFFF',
                color: C.GREEN,
            }
        ]

        const {dateTime, feeling, origin, energy, thoughts} = rowData
        const dayOfMonth = Moment(dateTime).format('D')
        const dayOfWeek = Moment(dateTime).format('ddd').toUpperCase()
        const time = Moment(dateTime).format('h:mm a').toUpperCase()

        return (
            <View>
            <Swipeout
                right={swipeoutBtns}
                backgroundColor="transparent"
                autoClose={false}
                open="right"
            >
                <TouchableHighlight
                    underlayColor='#dddddd'
                    onPress={() => this.selectMood(this.props.moods[rowID])}
                >
                    <View style={[styles.moodItemView, this.moodItemBackgroundColor(energy)]}>
                        <View style={[styles.moodItemDate, this.border('yellow')]}>
                            <Text style={styles.moodItemDateOfMonth}>{dayOfMonth}</Text>
                            <Text style={styles.moodItemDateOfWeek}>{dayOfWeek}</Text>
                        </View>
                        <View style={[styles.moodItemContent, this.border('blue')]}>
                            <Text style={styles.moodItemTime}>{time}</Text>
                            <Text style={styles.feeling}>{feeling}</Text>
                            <Text style={styles.thoughts} numberOfLines={2}>{thoughts}</Text>
                        </View>
                    </View>

                </TouchableHighlight>
            </Swipeout>
            </View>
        );
    }


    selectMood = (mood) => {
        this.props.selectMood(mood)
        this.props.navigator.push({name: 'moodDetail', title: 'Mood Detail', moodId: mood.id })
    }

    render() {

        return (
            <View style={styles.container}>

                <ListView
                    style={styles.list}
                    dataSource={this.state.dataSource}
                    renderRow={this.renderRow}
                />

                <Tabs
                    style={styles.tabs}
                    selected={this.state.tab}
                    selectedStyle={styles.selectedTab}
                    onSelect={el=>this.setState({tab: el.props.name})}
                >
                    <Text name="history">HISTORY</Text>
                    <Text name="more">MORE</Text>
                </Tabs>

                <ActionButton
                    buttonColor={C.GREEN}
                    position='center'
                    offsetY={16}
                    onPress={()=>this.props.navigator.push({name: 'moodEdit', title: 'New Entry'})}
                >

                </ActionButton>

            </View>
        )
    }

    moodItemBackgroundColor(energy){
        if(energy==='Catabolic'){
            return {backgroundColor: C.GREY}
        } else {
            return {backgroundColor: C.WHITE}
        }
    }

    border(color) {
        return {
            borderColor: color,
            borderWidth: 0
        }
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingLeft: 16,
        paddingTop: 8,
        paddingBottom: 8,
        borderBottomWidth: 1,
        borderBottomColor: '#e9e9e9',
    },
    feeling: {
        paddingTop: 2,
        paddingBottom: 2,
        fontSize: 20,
        fontFamily: C.FONTFAMILY,
        color: C.BLACK_TEXT_COLOR,
        fontWeight: 'bold'
    },
    thoughts: {
        fontSize: 14,
        color: C.BLACK_TEXT_COLOR,
        letterSpacing: -0.4,
        paddingBottom: 8
    },
    list: {

    },
    tabs: {
        backgroundColor: 'white',
        borderTopWidth: 1,
        borderTopColor: 'lightgrey'

    },
    selectedTab: {
        fontWeight: 'bold'
    },
    moodItem: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'stretch',
    },
    moodItemView: {
        flex: 1,
        flexDirection: 'row',
    },
    moodItemDate: {
        width: C.MOOD_ITEM_WIDTH,
        height: C.MOOD_ITEM_HEIGHT,
        justifyContent: 'center',
        alignItems: 'center',
    },
    moodItemContent: {
        flex: 1,
        height: C.MOOD_ITEM_HEIGHT,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#D0D0D0',
        paddingRight: 24
    },
    moodItemDateOfMonth: {
        fontSize: 20,
        color: C.GREY_TEXT_COLOR,
    },
    moodItemDateOfWeek: {
        paddingTop: 8,
        fontSize: 14,
        color: C.BLACK_TEXT_COLOR,
        fontWeight: 'bold'
    },
    moodItemTime: {
        fontSize: 12,
        color: C.GREY_TEXT_COLOR,
        paddingTop: 4,
        fontWeight: 'bold'
    },
})


function mapStateToProps(state) {
    return {
        moods: state.moods,
        global: state.global,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        selectMood: selectMood,
        setGlobalBackground: setGlobalBackground
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MoodList)
