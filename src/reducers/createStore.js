import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import MoodActiveReducer from './mood-active-reducer'
import MoodsReducer from './moods-reducer'
import GlobalReducer from './global-reducer'

const middleware = applyMiddleware(thunk);

export default (data = {}) => {
    const rootReducer = combineReducers({
        moods: MoodsReducer,
        moodActive: MoodActiveReducer,
        global: GlobalReducer
    })

    return createStore(rootReducer, data, middleware)
}

