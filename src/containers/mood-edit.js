'use strict'

import React, {
    Component,
    StyleSheet,
    View,
    Text,
    TouchableHighlight
} from 'react-native'

import Accordion from 'react-native-collapsible/Accordion'
import ActionButton from 'react-native-action-button'
import Icon from 'react-native-vector-icons/Ionicons'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as C from './constants'
import {
    //selectMood,
    //setGlobalBackground
} from '../actions/actions'

var SECTIONS = [
    {
        title: 'First',
        content: 'Lorem ipsum...',
    },
    {
        title: 'Second',
        content: 'Lorem ipsum...',
    },
    {
        title: 'Third',
        content: 'Lorem ipsum...',
    }
];


class MoodEdit extends Component {

    render() {
        return(
            <View style={[styles.container, this._border('blue')]}>
                <Accordion
                    sections={SECTIONS}
                    initiallyActiveSection={0}
                    renderHeader={this._renderHeader}
                    renderContent={this._renderContent}
                >
                </Accordion>
            </View>
        )
    }

    _renderHeader = (section, index, isActive) => {
        return (
            <View style={styles.header}>
                <Text style={styles.headerText}>test</Text>
            </View>
        );
    }


    _renderContent = (content, index, isActive) => {
        return (
            <View style={styles.content}>
                <Text>Some content...</Text>
            </View>
        );
    }

    _border(color) {
        return {
            borderColor: color,
            borderWidth: 4
        }
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start'
    },
    title: {
        textAlign: 'center',
        fontSize: 22,
        fontWeight: '300',
        marginBottom: 20,
    },
    header: {
        backgroundColor: '#F5FCFF',
        padding: 10,
    },
    headerText: {
        textAlign: 'center',
        fontSize: 16,
        fontWeight: '500',
    },
    content: {
        flex: 1,
        padding: 20,
        backgroundColor: '#fff',
    },
    active: {
        backgroundColor: 'rgba(255,255,255,1)',
    },
    inactive: {
        backgroundColor: 'rgba(245,252,255,1)',
    },
})

function mapStateToProps(state) {
    return {
        //moods: state.moods,
        //global: state.global
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        //selectMood: selectMood,
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MoodEdit)


