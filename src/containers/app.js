"use strict";

import React, {
    Component,
    StyleSheet,
    View,
    Text,
    Navigator,
    TouchableHighlight
} from 'react-native'

import {connect} from 'react-redux'
import Icon from 'react-native-vector-icons/Ionicons'

import MoodList from './mood-list'
import MoodDetail from './mood-detail'
import MoodEdit from './mood-edit'

var ROUTES = {
    moodList: MoodList,
    moodDetail: MoodDetail,
    moodEdit: MoodEdit
}

const GREEN = '#8BC129'

class App extends Component {

    constructor(props) {
        super(props);
    }

    renderScene = (route, navigator) => {
        var Component = ROUTES[route.name]
        return <Component route={route} navigator={navigator}/>
    }

    navigationBarRouteMapper = () => {
        return {
            LeftButton: function (route, navigator, index, navState) {
                if(index===0) return;
                return (
                    <TouchableHighlight
                        hitSlop={{top: 200, left: 200, bottom: 200, right: 200}}
                        onPress={() => {
                          if (index > 0) {
                            navigator.pop();
                          }
                        }}>
                        <Icon style={styles.icon} name="chevron-left">   </Icon>
                    </TouchableHighlight>
                )
            },

            RightButton: function (route, navigator, index, navState) {
                return null;
            },

            Title: function (route, navigator, index, navState) {
                return (
                    <Text style={styles.title}>
                        {route.title}
                    </Text>
                );

            }
        }
    }


    render() {
        return (

            <Navigator
                style={[styles.container, {backgroundColor: this.props.global.backgroundColor}]}
                initialRoute={{name: 'moodList', title: 'MindMastery'}}
                renderScene={this.renderScene}
                configureScene={() => { return Navigator.SceneConfigs.FloatFromRight }}
                sceneStyle={{paddingTop: Navigator.NavigationBar.Styles.General.TotalNavHeight}}

                navigationBar={
                    <Navigator.NavigationBar
                      routeMapper={this.navigationBarRouteMapper()}
                      navigationStyles={Navigator.NavigationBar.StylesIOS}

                    />
                }
            />

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    title: {
        color: GREEN,
        fontSize: 20
    },
    icon: {
        marginTop: 5,
        marginLeft: 12,
        color: GREEN,
        fontSize: 20,
        alignSelf: 'center'
    },
    navBar: {
        backgroundColor: 'transparent'
    }
});


function mapStateToProps(state) {
    return {
        moods: state.moods,
        global: state.global,
    }
}


export default connect(mapStateToProps)(App)

