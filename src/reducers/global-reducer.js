import { SET_GLOBAL_BACKGROUND } from '../actions/constants'

export default function(state = {}, action) {
    switch(action.type) {
        case SET_GLOBAL_BACKGROUND:
            return {...state, backgroundColor: action.payload}
    }
    return state
}