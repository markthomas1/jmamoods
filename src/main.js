"use strict";

import React, {
    Component,
    StyleSheet,
    View,
    Text,
    Navigator,
    TouchableHighlight
} from 'react-native'

import createStore from './reducers/createStore'
import {Provider} from 'react-redux'
import App from './containers/app'

const store = createStore()

export default class Main extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Provider store={store}>
                <App />
            </Provider>
        )
    }
}

const styles = StyleSheet.create({

});
