//we are using namespaceing to prevent module's action type collision
//every module should have a unique name. the best practice is to set name
//base on module's name

//name of this modules
export const NAME = 'jma'

//action types
export const SELECT_MOOD = `${NAME}/SELECT_MOOD`
export const SET_GLOBAL_BACKGROUND = `${NAME}/SET_GLOBAL_BACKGROUND`

//as you can see above, each action is namespaced with module's name.
