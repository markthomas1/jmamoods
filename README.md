npm i
npm start (built-in package manager, like webpack but for react-native)

open ios xcode project in xcode and hit the play button to start the emulator

start genymotion android emulator
react-native run-android

-------


Releasing on Android:
=====================

https://facebook.github.io/react-native/docs/signed-apk-android.html

keytool -genkey -v -keystore android-release-key.keystore -alias android-key-alias -keyalg RSA -keysize 2048 -validity 10000

alias: android-key-alias
pw: mas2016

cd android && ./gradlew installRelease

